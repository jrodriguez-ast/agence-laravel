'use strict';
/**
 * Created by jose on 11/09/15.
 */
app.controller('GraficoController', ['$injector', '$scope', 'rVentas', function ($injector, $scope, rVentas) {
    // Este proceso enredado es para generar el grafico. pufff
    $scope.grafico = {
        series: [],
        labels: [],
        data: []
    };
    var data = null, rows = {}, graf = $scope.grafico, series = $scope.model.vendedores, i,len;

    for (i = 0, len = rVentas.length; i < len; i++) {
        data = rVentas[i].data;

        graf.labels.push(rVentas[i].date);
        for (var s = 0, lenS = series.length; s < lenS; s++) {
            if(angular.isUndefined(rows[series[s].co_usuario])){
                rows[series[s].co_usuario] = [];
            }

            for (var d = 0, lenD = data.length; d < lenD; d++) {
                if(series[s].co_usuario == data[d].co_usuario){
                    rows[series[s].co_usuario].push(data[d].liquida);
                }
            }
        }
    }

    for (i = 0, len = series.length; i < len; i++) {
        var serie = series[i];
        graf.data.push(rows[serie.co_usuario]);
        graf.series.push(serie.no_usuario);
    }
}]);