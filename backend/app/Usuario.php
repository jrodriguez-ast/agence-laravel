<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Usuario extends Model
{
    protected $table = 'cao_usuario';
    protected $primaryKey = 'co_usuario';

    public function getVendedores()
    {
        return $this->hasMany('\App\Permiso', 'co_usuario', 'co_usuario')
            ->where('co_sistema', 1)
            ->where('in_ativo', 'S')
            ->whereIn('co_tipo_usuario', [0, 1, 2]);
    }

    public function getVendedoresJoin()
    {
        return DB::table($this->table)
            ->join('permissao_sistema AS perm', "{$this->table}.{$this->primaryKey}", '=', "perm.{$this->primaryKey}")
            ->where('co_sistema', 1)->where('in_ativo', 'S')
            ->whereIn('co_tipo_usuario', [0, 1, 2])
            ->select(["{$this->table}.{$this->primaryKey}", "{$this->table}.no_usuario"])
            ->get();
    }

}
