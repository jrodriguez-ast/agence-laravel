<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Factura extends Model
{
    protected $table = 'cao_fatura';
    protected $primaryKey = 'co_fatura';

    /**
     * @param $filtros
     * @return mixed
     */
    public function getRelatorio($filtros)
    {
        $date = new Carbon();
        $desde = (empty($filtros['desde'])) ? $date->createFromDate(1985, 01, 01) : $filtros['desde'];

        $hasta = $date->now();
        if (!empty($filtros['hasta'])) {
            list($y, $m, $d) = explode('-', $filtros['hasta']);
            $d = substr($d, 0, 2);
            $hasta = $date->createFromDate($y, $m, $d)->addMonth()->subDay();
        }

        $result = [];
        foreach ($filtros['vendedores'] AS $vendedor) {
            $tmp['vendedor'] = Usuario::findOrNew($vendedor['co_usuario'], ['no_usuario']);
            $tmp['data'] = DB::table("$this->table AS fac")
                ->join('cao_cliente AS cli', 'fac.co_cliente', '=', 'cli.co_cliente')
                ->join('cao_sistema AS sis', 'fac.co_sistema', '=', 'sis.co_sistema')
                ->join('cao_os AS os', 'fac.co_os', '=', 'os.co_os')
                ->join('cao_usuario AS usu', 'usu.co_usuario', '=', 'os.co_usuario')
                ->join('cao_salario AS sal', 'sal.co_usuario', '=', 'usu.co_usuario')
                ->whereBetween('data_emissao', [$desde, $hasta])
                ->where('usu.co_usuario', $vendedor['co_usuario'])
                ->select([
                    DB::raw('MONTHNAME(str_to_date(MONTH(data_emissao), \'%m\')) AS mes'),
                    DB::raw('MONTH(data_emissao) AS mes_numero'),
                    DB::raw('YEAR(data_emissao) AS anio'),
                    DB::raw('ROUND(SUM(fac.valor * (fac.total_imp_inc/100)),2) AS liquida'),
                    DB::raw('ROUND(SUM(fac.valor * (fac.total_imp_inc/100) * ( fac.comissao_cn/100)),2) AS comision'),
                    'sal.brut_salario AS costo'
                ])
                ->orderBy('anio', 'asc')
                ->orderBy('mes_numero', 'asc')
                ->groupBy('anio')
                ->groupBy('mes')
                ->groupBy('mes_numero')
                ->groupBy('sal.brut_salario')
                ->distinct()
                ->get();

            array_push($result, $tmp);
        }

        return $result;
    }

    public function getGrafico($filtros)
    {
        $date = new Carbon();
        $desde = $date->createFromDate(1985, 01, 01);
        if (!empty($filtros['desde'])) {
            list($y, $m, $d) = explode('-', $filtros['desde']);
            $d = substr($d, 0, 2);
            $desde = $date->createFromDate($y, $m, $d);
        }

        $hasta = $date->now();
        if (!empty($filtros['hasta'])) {
            list($y, $m, $d) = explode('-', $filtros['hasta']);
            $d = substr($d, 0, 2);
            $hasta = $date->createFromDate($y, $m, $d)->addMonth()->subDay();
        }

        $vendedores = [];
        foreach ($filtros['vendedores'] AS $vendedor) {
            array_push($vendedores, $vendedor['co_usuario']);
        }

        // CAlculo los intervalos
        $result = [];
        do {
            $hastaQuery = $desde->copy();
            $hastaQuery->addMonth()->subDay();
            $tmp['date'] = "{$desde->month} - {$desde->year}";
            $tmp['data'] = DB::table("$this->table AS fac")
                ->join('cao_cliente AS cli', 'fac.co_cliente', '=', 'cli.co_cliente')
                ->join('cao_sistema AS sis', 'fac.co_sistema', '=', 'sis.co_sistema')
                ->join('cao_os AS os', 'fac.co_os', '=', 'os.co_os')
                ->leftjoin('cao_usuario AS usu', 'usu.co_usuario', '=', 'os.co_usuario')
                ->join('cao_salario AS sal', 'sal.co_usuario', '=', 'usu.co_usuario')
                ->whereBetween('data_emissao', [$desde, $hastaQuery])
                ->whereIn('usu.co_usuario', $vendedores)
                ->select([
                    DB::raw('ROUND(SUM(fac.valor * (fac.total_imp_inc/100)),2) AS liquida'),
                    'sal.brut_salario AS costo',
                    'usu.no_usuario',
                    'usu.co_usuario'
                ])
                ->orderBy('usu.no_usuario', 'asc')
                ->groupBy('sal.brut_salario')
                ->groupBy('usu.no_usuario')
                ->groupBy('usu.co_usuario')
                ->distinct()
                ->get();

            if (!empty($tmp['data'])) {
                array_push($result, $tmp);
            }
            $desde->addMonth();
        } while ($desde->diffInMonths($hasta) > 0);
        return $result;
    }

    public function getPizza($filtros)
    {
        $date = new Carbon();
        $desde = $date->createFromDate(1985, 01, 01);
        if (!empty($filtros['desde'])) {
            list($y, $m, $d) = explode('-', $filtros['desde']);
            $d = substr($d, 0, 2);
            $desde = $date->createFromDate($y, $m, $d);
        }

        $hasta = $date->now();
        if (!empty($filtros['hasta'])) {
            list($y, $m, $d) = explode('-', $filtros['hasta']);
            $d = substr($d, 0, 2);
            $hasta = $date->createFromDate($y, $m, $d)->addMonth()->subDay();
        }

        $vendedores = [];
        foreach ($filtros['vendedores'] AS $vendedor) {
            array_push($vendedores, $vendedor['co_usuario']);
        }

        return DB::table("$this->table AS fac")
            ->join('cao_cliente AS cli', 'fac.co_cliente', '=', 'cli.co_cliente')
            ->join('cao_sistema AS sis', 'fac.co_sistema', '=', 'sis.co_sistema')
            ->join('cao_os AS os', 'fac.co_os', '=', 'os.co_os')
            ->join('cao_usuario AS usu', 'usu.co_usuario', '=', 'os.co_usuario')
            ->join('cao_salario AS sal', 'sal.co_usuario', '=', 'usu.co_usuario')
            ->whereBetween('data_emissao', [$desde, $hasta])
            ->whereIn('usu.co_usuario', $vendedores)
            ->select([
                DB::raw('ROUND(SUM(fac.valor * (fac.total_imp_inc/100)),2) AS liquida'),
                'usu.no_usuario',
            ])
            ->orderBy('usu.no_usuario', 'asc')
            ->groupBy('usu.no_usuario')
            ->distinct()
            ->get();

    }

}
