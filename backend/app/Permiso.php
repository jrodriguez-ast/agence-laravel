<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'permissao_sistema';
    protected $primaryKey = 'co_usuario';

    public function vendedores()
    {
        return $this->belongsTo('\App\Usuario', 'co_usuario', 'co_usuario');
    }
}
