<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/vendedores', function () {
    $vendedores = new \App\Usuario();
    $data = $vendedores->getVendedoresJoin();
    return response()->json($data, 200);
});

Route::post('/relatorio', function (\Illuminate\Http\Request $request) {
    $factura = new \App\Factura();
    $data = $factura->getRelatorio($request->all());
    return response()->json($data, 200);
});

Route::post('/grafico', function (\Illuminate\Http\Request $request) {
    $factura = new \App\Factura();
    $data = $factura->getGrafico($request->all());
    return response()->json($data, 200);
});

Route::post('/pizza', function (\Illuminate\Http\Request $request) {
    $factura = new \App\Factura();
    $data = $factura->getPizza($request->all());
    return response()->json($data, 200);
});